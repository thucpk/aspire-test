# Aspire test

## Assumptions

- Recommended data input: each line is a valid json value. Good performance for read, parser file
- Assume uuid as universally unique identifier event
- Assume data from each endpoint has difference format/structure
- Assume the remaining fields has same format/structure

## Preprocess

- We have 5 endpoint: invoices, payments, balance-sheet, bank-transactions, profit-and-loss
- Endpoint profit-and-loss and balance-sheet return sheet format data
- Endpoint invoices, payments and bank-transactions return json format data
- Integration item details contain json format data
- Organizations database depend on your business. I recommended split data to 7 table: data transaction (1), each table for data return from endpoint (5) and integration_item_details (1). Because of relationship between data transaction and data from endpoint, I will choose a RDBMS.
- I agree to use python language, it's clear, readable syntax, quick progression, versatility,widely available resources. Using it carefully, because python has limit/slow speed :)

## Requirement
- python 2.7/3.6
- pandas 0.23.4
- ijson (option) - needed if big json file :)

## Enjoy
python main.py
