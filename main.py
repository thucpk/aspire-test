import pandas as pd
from copy import deepcopy


def add_uuid_to_df(df, uuid):
    df['uuid'] = uuid
    return df


def union_dfs_with_uuid(df, ref_col='integration_item_details'):
    return pd.concat(
        df.apply(
            lambda x: add_uuid_to_df(x[ref_col], x['uuid']),
            axis=1
        ).values.tolist(),
        ignore_index=True, sort=False)


def parse_sheet_data_endpoint(data):
    try:
        rs = list()
        header = list()
        for row in data:
            if row['RowType'] == 'Header':
                header = list(map(lambda x: x['Value'], row['Cells'][1:]))
            else:
                title = row.get('Title', 'Title_NULL')
                head = deepcopy(header)
                head.append(title)
                if not row.get('Rows'):
                    row['Rows'] = []
                for sr in row['Rows']:
                    r = deepcopy(head)
                    for v in sr['Cells']:
                        r.append(v.get('Value'))
                        r.append(v.get('Attributes'))
                    rs.append(r)
        return rs
    except Exception as e:
        print(e)


def parse_json_data_endpoint(data):
    return pd.DataFrame(data)


if __name__ == '__main__':
    sheet_endpoint = ['profit-and-loss', 'balance-sheet']

    in_file = 'in/Data Engineer Challenge.json'

    df_raw = pd.read_json(in_file)
    df_raw_item_details = df_raw['integration_item_details'].map(parse_json_data_endpoint).reset_index()
    df_raw_item_details['uuid'] = df_raw['uuid']
    df_integration_item_details = union_dfs_with_uuid(df_raw_item_details)

    endpoints = set(df_raw['endpoint'].tolist())
    for endpoint in endpoints:
        print(endpoint)
        df_lookup_raw = df_raw.loc[df_raw['endpoint'] == endpoint]
        if endpoint not in sheet_endpoint:
            values = df_lookup_raw['data'].values.tolist()
            df_endpoint = parse_json_data_endpoint(values)
            df_endpoint['uuid'] = df_lookup_raw['uuid'].tolist()
        else:
            df_endpoint = df_lookup_raw['data'].map(parse_sheet_data_endpoint).map(pd.DataFrame).reset_index()
            df_endpoint['uuid'] = df_lookup_raw['uuid'].tolist()
            df_endpoint = union_dfs_with_uuid(df_endpoint, 'data')
        df_endpoint.to_csv("out/endpoint_{0}.csv".format(endpoint), index=False)

    df_raw.drop(['data', 'integration_item_details'], axis=1).to_csv("out/transaction.csv", index=False)
